export default {
  path: '/auth',
  name: 'auth',
  redirect: { name: 'auth.login' },
  component: () => import(/* webpackChunkName: "auth" */ '@/views/Auth'),
  children: [
    {
      path: 'login',
      name: 'auth.login',
      component: () =>
        import(/* webpackChunkName: "auth" */ '@/views/Auth/LoginPage')
    },
    {
      path: 'register',
      name: 'auth.register',
      component: () =>
        import(/* webpackChunkName: "auth" */ '@/views/Auth/RegisterPage')
    },
    {
      path: 'forgot-password',
      name: 'auth.forgot-password',
      component: () =>
        import(/* webpackChunkName: "auth" */ '@/views/Auth/ForgotPassword')
    }
  ]
}
